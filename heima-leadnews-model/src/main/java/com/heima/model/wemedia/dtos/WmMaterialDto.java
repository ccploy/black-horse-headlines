package com.heima.model.wemedia.dtos;

import com.heima.model.common.dtos.PageRequestDto;
import lombok.Data;

/**
 * @author CC
 * time 2024-05-06
 * description
 */
@Data
public class WmMaterialDto extends PageRequestDto {
    /**
     * 0未收藏
     * 1已收藏
     */
    private Short isCollection;
}
