package com.heima.model.admin.pojos;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author CC
 * time 2024-06-04
 * description

 */
@Data
public class AdSensitive  implements Serializable
{
    private static final long serialVersionUID =1L;

    private Integer id;

    private String sensitives;

    private Date createdTime;




}
