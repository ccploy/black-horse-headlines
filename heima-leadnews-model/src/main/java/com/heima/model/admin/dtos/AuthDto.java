package com.heima.model.admin.dtos;

import lombok.Data;

import java.io.Serializable;

/**
 * @author CC
 * time 2024-06-04
 * description
 */
@Data
public class AuthDto implements Serializable {


    private Integer id;

    private String msg;

    private Integer page;

    private Integer size;

    private Integer status;



}
