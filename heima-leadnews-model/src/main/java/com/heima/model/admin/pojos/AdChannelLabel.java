package com.heima.model.admin.pojos;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @author CC
 * time 2024-06-04
 * description
 */
@Data
@TableName("ad_channel_label")
public class AdChannelLabel implements Serializable {

    private Integer id;

    private Integer channelId;

    private Integer labelId;

    private Integer ord;

}
