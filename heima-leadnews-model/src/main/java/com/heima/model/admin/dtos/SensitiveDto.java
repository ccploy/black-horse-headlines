package com.heima.model.admin.dtos;

import lombok.Data;

import java.io.Serializable;

/**
 * @author CC
 * time 2024-06-04
 * description
 */
@Data
public class SensitiveDto implements Serializable {

    private static final long serialVersionUID =1L;

    private String name;

    private Integer page;

    private Integer size;

}
