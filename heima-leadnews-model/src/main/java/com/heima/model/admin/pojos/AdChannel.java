package com.heima.model.admin.pojos;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author CC
 * time 2024-06-04
 * description
 */
@Data
public class AdChannel implements Serializable {

    private Integer id;

    private Date createTime;

    private String description;

    private String name;

    private Integer ord;

    private Boolean status;

    private Boolean isDefault;





}
