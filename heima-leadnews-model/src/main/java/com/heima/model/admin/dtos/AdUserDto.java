package com.heima.model.admin.dtos;

import lombok.Data;

import java.io.Serializable;

/**
 * @author CC
 * time 2024-06-03
 * description
 */
@Data
public class AdUserDto implements Serializable {
    private static final long serialVersionUID =1L;

    private String name;

    private String password;


}
