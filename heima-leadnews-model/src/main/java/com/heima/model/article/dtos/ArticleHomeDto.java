package com.heima.model.article.dtos;

import lombok.Data;

import java.util.Date;

/**
 * @author CC
 * time 2024-05-05
 * description
 */
@Data
public class ArticleHomeDto {

    Date maxBehotTime;
    Date minBehotTime;
    String tag;
    Integer size;


}
