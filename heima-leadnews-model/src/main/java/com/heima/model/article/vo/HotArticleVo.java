package com.heima.model.article.vo;

import com.heima.model.article.pojos.ApArticle;
import lombok.Data;

import java.io.Serializable;

/**
 * @author CC
 * time 2024-06-08
 * description
 */
@Data
public class HotArticleVo extends ApArticle {

    private Integer score;

}
