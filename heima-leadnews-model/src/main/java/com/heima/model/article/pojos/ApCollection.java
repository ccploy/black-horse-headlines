package com.heima.model.article.pojos;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author CC
 * time 2024-06-08
 * description
 */
@Data
public class ApCollection implements Serializable {

    private static final long serialVersionUID =1L;

    private Long id;
    /**
     * 实体id
     */
    private Integer entryId;
    /**
     * 文章id
     */
    private Long articleId;
    /**
     * 点赞内容类型
     * 0 文章
     * 1 动态
     */
    private Short type;

    private Date collection_time;

    private Date publish_time;


}
