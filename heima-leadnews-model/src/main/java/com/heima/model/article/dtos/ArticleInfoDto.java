package com.heima.model.article.dtos;

import com.heima.model.comon.IdEncrypt;
import lombok.Data;

import java.io.Serializable;

/**
 * @author CC
 * time 2024-06-08
 * description
 */
@Data
public class ArticleInfoDto implements Serializable {
    private static final long serialVersionUID =1L;
    @IdEncrypt
    private Long articleId;
    private Integer authorId;

}
