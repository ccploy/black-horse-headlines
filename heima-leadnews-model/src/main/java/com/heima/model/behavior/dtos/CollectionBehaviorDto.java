package com.heima.model.behavior.dtos;

import com.heima.model.comon.IdEncrypt;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author CC
 * time 2024-06-08
 * description
 */
@Data
public class CollectionBehaviorDto implements Serializable {

    private static final long serialVersionUID =1L;
    /**
     * 文章id
     */
    @IdEncrypt
    private Long entryId;
    /**
     * 0收藏    1取消收藏
     */
    private Short operation;

    private Date publishedTime;
    /**
     * 0文章    1动态
     */
    private Short type;


    }
