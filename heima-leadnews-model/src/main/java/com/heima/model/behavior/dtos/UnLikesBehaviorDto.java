package com.heima.model.behavior.dtos;

import com.heima.model.comon.IdEncrypt;
import lombok.Data;

import java.io.Serializable;

/**
 * @author CC
 * time 2024-06-08
 * description
 */
@Data
public class UnLikesBehaviorDto implements Serializable {

    private static final long serialVersionUID =1L;
    @IdEncrypt
    private Long articleId;
    //0 不喜欢      1 取消不喜欢
    private Short type;


}
