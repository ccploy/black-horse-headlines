package com.heima.model.behavior.dtos;

import com.heima.model.comon.IdEncrypt;
import lombok.Data;

import java.io.Serializable;

/**
 * @author CC
 * time 2024-06-08
 * description
 */
@Data
public class LikesBehaviorDto implements Serializable {

    private static final long serialVersionUID =1L;
    /**
     * 文章id
     */
    @IdEncrypt
    private Long articleId;
    /**
     * 0 点赞   1 取消点赞
     */
    private Short operation;
    /**
     * 0文章  1动态   2评论
     */
    private Short type;



}
