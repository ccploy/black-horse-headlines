package com.heima.model.behavior.dtos;

import com.heima.model.comon.IdEncrypt;
import lombok.Data;

import java.io.Serializable;

/**
 * @author CC
 * time 2024-06-08
 * description
 */
@Data
public class UserRelationDto implements Serializable {

    private static final long serialVersionUID =1L;
    @IdEncrypt
    private Long articleId;

    private Integer authorId;
    /**
     * 0  关注   1  取消
     */
    private Short operation;


}
