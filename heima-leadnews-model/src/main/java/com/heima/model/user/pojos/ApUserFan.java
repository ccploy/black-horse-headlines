package com.heima.model.user.pojos;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author CC
 * time 2024-06-08
 * description
 */
@Data
public class ApUserFan implements Serializable {

    private static final long serialVersionUID =1L;

    private Integer id;


    private Integer userId;


    private Integer fansId;

    private String fansName;
    /**
     * 粉丝忠诚度
     * 0正常
     * 1潜力股
     * 2勇士
     * 3铁杆
     * 4老铁
     */
    private Short level;

    private Date createTime;
    /**
     * 是否可见我的动态
     */
    private Short isDisplay;
    /**
     * 是否屏蔽私信
     */
    private Short isShieldLetter;
    /**
     * 是否屏蔽评论
     */
    private Short isShieldComment;

}
