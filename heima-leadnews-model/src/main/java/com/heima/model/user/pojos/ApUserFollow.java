package com.heima.model.user.pojos;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author CC
 * time 2024-06-08
 * description
 *   用户关注列表
 */
@Data
public class ApUserFollow implements Serializable {

    private static final long serialVersionUID =1L;

    private Integer id;
    /**
     * 用户ID
     */
    private Integer userId;
    /**
     * 作者id
     */
    private Integer followId;
    /**
     * 关注级别
     *  0 偶尔感兴趣
     *  1 一般
     *  2 经常
     *  3 高度
     */
    private Short level;
    /**
     *是否动态通知
     *  0 否
     *  1 是
     */
    private Short isNotice;

    private Date createdTime;



}
