package com.heima.model.seach.dtos;

import lombok.Data;

@Data
public class HistorySearchDto {
    /**
    * 接收搜索历史记录id
    */
    String id;
}