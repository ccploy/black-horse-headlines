package com.heima.schedule.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.schedule.dtos.Task;
import com.heima.model.schedule.pojos.Taskinfo;

/**
 * @author CC
 * time 2024-05-08
 * description
 */
public interface TaskService extends IService<Taskinfo> {
    long addTask(Task task);

    boolean cancelTask(long taskId);
    Task poll(int type,int priority);

}
