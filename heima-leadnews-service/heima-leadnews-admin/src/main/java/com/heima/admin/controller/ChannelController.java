package com.heima.admin.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.admin.service.AdChannelLabelService;
import com.heima.apis.wemedia.IWmChannelClient;
import com.heima.model.admin.pojos.AdChannel;
import com.heima.model.admin.pojos.AdChannelLabel;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.dtos.ChannelDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author CC
 * time 2024-06-04
 * description
 */
@RestController
@RequestMapping("/api/v1/channel")
public class ChannelController {

    @Autowired
    private AdChannelLabelService adChannelLabelService;

    @Autowired
    private IWmChannelClient iWmChannelClient;

    @GetMapping("/del/{id}")
    public ResponseResult del(@PathVariable("id")Integer id){
        ResponseResult del = iWmChannelClient.del(id);
        if (del.getData()== AppHttpCodeEnum.SUCCESS) {
           adChannelLabelService.remove(Wrappers.lambdaQuery(AdChannelLabel.class)
                   .eq(AdChannelLabel::getChannelId,id));
        }
        return del;
    }
    @PostMapping("/api/v1/channel/list")
    public ResponseResult list(@RequestBody ChannelDto dto){
        return iWmChannelClient.list(dto);
    }

    @PostMapping("/api/v1/channel/save")
    public ResponseResult save(@RequestBody AdChannel data){
        return iWmChannelClient.save(data);
    }

    @PostMapping("/api/v1/channel/update")
    public  ResponseResult update(@RequestBody AdChannel data){
        if (data==null || data.getId()==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //查看频道是否被引用
        Integer channelId = data.getId();
        int count = adChannelLabelService.count(Wrappers.lambdaQuery(AdChannelLabel.class)
                .eq(AdChannelLabel::getChannelId, channelId));
        if (count>0){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_IS_USING,"频道被引用，不能修改");
        }
        return iWmChannelClient.update(data);
    }

}
