package com.heima.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.admin.pojos.AdChannelLabel;

/**
 * @author CC
 * time 2024-06-04
 * description
 */
public interface AdChannelLabelService extends IService<AdChannelLabel> {
}
