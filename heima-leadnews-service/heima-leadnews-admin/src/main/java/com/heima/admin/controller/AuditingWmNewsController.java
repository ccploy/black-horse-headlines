package com.heima.admin.controller;

import com.heima.apis.wemedia.IWmNewsClient;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.NewsAuthDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author CC
 * time 2024-06-05
 * description
 */
@RestController
@RequestMapping("/api/v1/news")
public class AuditingWmNewsController {

    @Autowired
    private IWmNewsClient iWmNewsClient;

    @PostMapping("/auth_fail")
    public ResponseResult auth_fail(@RequestBody NewsAuthDto dto){
        return iWmNewsClient.authPass(dto);
    }
    @PostMapping("/auth_pass")
    public ResponseResult auth_pass(@RequestBody NewsAuthDto dto){
        return iWmNewsClient.authPass(dto);
    }
    @GetMapping("/one_vo/{id}")
    public ResponseResult showDetails(@PathVariable("id")Integer id){
        return iWmNewsClient.showDetails(id);
    }
    @PostMapping("/api/v1/news/list_vo")
    public ResponseResult list(@RequestBody NewsAuthDto dto){
        return iWmNewsClient.getList(dto);
    }


}
