package com.heima.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.admin.dtos.AdUserDto;
import com.heima.model.admin.pojos.AdUser;
import com.heima.model.common.dtos.ResponseResult;

/**
 * @author CC
 * time 2024-06-03
 * description
 */
public interface AdUserService extends IService<AdUser> {

    ResponseResult login(AdUserDto dto);
}
