package com.heima.admin.controller;

import com.heima.apis.wemedia.IWmSensitiveClient;
import com.heima.model.admin.dtos.SensitiveDto;
import com.heima.model.admin.pojos.AdSensitive;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author CC
 * time 2024-06-04
 * description
 */
@RestController
@RequestMapping("/api/v1/sensitive")
public class SensitiveController {

    @Autowired
    private IWmSensitiveClient iWmSensitiveClient;

    @DeleteMapping("/del/{id}`")
    public ResponseResult del(@PathVariable("id")Integer id){
        return iWmSensitiveClient.del(id);
    }
    @PostMapping("/list")
    public ResponseResult list(@RequestBody SensitiveDto dto) {
        return iWmSensitiveClient.list(dto);
    }

    @PostMapping("/update")
    public ResponseResult update(@RequestBody AdSensitive adSensitive) {
        return iWmSensitiveClient.update(adSensitive);
    }

    @PostMapping("/save")
    public ResponseResult save(@RequestBody AdSensitive adSensitive) {
        return iWmSensitiveClient.save(adSensitive);
    }







}
