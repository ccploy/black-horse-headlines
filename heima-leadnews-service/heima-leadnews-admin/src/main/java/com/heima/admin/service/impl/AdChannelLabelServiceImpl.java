package com.heima.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.admin.mapper.AdChannelLabelMapper;
import com.heima.admin.service.AdChannelLabelService;
import com.heima.model.admin.pojos.AdChannelLabel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author CC
 * time 2024-06-04
 * description
 */
@Service
@Slf4j
public class AdChannelLabelServiceImpl extends ServiceImpl<AdChannelLabelMapper, AdChannelLabel> implements AdChannelLabelService {




}
