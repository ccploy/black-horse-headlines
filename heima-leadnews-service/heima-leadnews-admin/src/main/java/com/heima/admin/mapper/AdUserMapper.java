package com.heima.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.admin.pojos.AdUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author CC
 * time 2024-06-03
 * description
 */
@Mapper
public interface AdUserMapper extends BaseMapper<AdUser> {
}
