package com.heima.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.admin.pojos.AdChannelLabel;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author CC
 * time 2024-06-04
 * description
 */
@Mapper
public interface AdChannelLabelMapper extends BaseMapper<AdChannelLabel> {
}
