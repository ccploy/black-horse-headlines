package com.heima.admin.controller;

import com.heima.apis.user.IApUserClient;
import com.heima.apis.wemedia.IWmNewsClient;
import com.heima.model.admin.dtos.AuthDto;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author CC
 * time 2024-06-04
 * description
 * 用户审核
 */
@RestController
@RequestMapping("/api/v1/auth")
public class AuditingUserController {

    @Autowired
    private IApUserClient iApUserClient;

    /**
     * - 在app端的个人中心用户可以实名认证，需要材料为：姓名、身份证号、身份证正面照、身份证反面照、手持照片、
     * 活体照片（通过**微笑、眨眼、张嘴、摇头、点头**等组合动作，确保操作的为真实活体人脸。），
     * 当用户提交审核后就到了后端让运营管理人员进行审核
     * - 平台运营端查看用户认证信息，进行审核，其中审核包括了用户身份审核，需要对接公安系统校验身份证信息
     * - 用户通过审核后需要开通自媒体账号（该账号的用户名和密码与app一致）
     * <p>
     * 分页查询认证列表
     *
     * @param dto
     * @return
     */
    @PostMapping("/list")
    public ResponseResult list(@RequestBody AuthDto dto) {
        if (dto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        return iApUserClient.listApUsers(dto);
    }
    @PostMapping("/authFail")
    public ResponseResult authFail(@RequestBody AuthDto dto){
        return iApUserClient.authFail(dto);
    }
    @PostMapping("/authPass")
    public ResponseResult authPass(@RequestBody AuthDto dto){
        return iApUserClient.authPass(dto);
    }




}
