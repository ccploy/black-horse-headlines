package com.heima.search.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.seach.dtos.HistorySearchDto;
import com.heima.search.pojos.ApUserSearch;

public interface ApUserSearchService  {
    /**
     * 保存用户搜索记录
     * @param keyword
     * @param uid
     */
    void  insert(String keyword,Integer uid);

    ResponseResult findUserSearch();

    ResponseResult delUserSearch(HistorySearchDto historySearchDto);
}