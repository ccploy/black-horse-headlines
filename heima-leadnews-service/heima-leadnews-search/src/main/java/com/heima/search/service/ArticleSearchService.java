package com.heima.search.service;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.seach.dtos.UserSearchDto;

import java.io.IOException;

public interface ArticleSearchService {

    /**
     ES文章分页搜索
     @return
     */
    ResponseResult search(UserSearchDto userSearchDto) throws IOException;
}