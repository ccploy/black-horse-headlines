package com.heima.user.controller.v1;

import com.heima.model.behavior.dtos.UserRelationDto;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.user.service.ApUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author CC
 * time 2024-06-08
 * description
 */
@RestController
@RequestMapping("/api/v1/user")
public class ApUserController {

    @Autowired
    private ApUserService apUserService;

    @PostMapping("/user_follow")
    public ResponseResult follow(@RequestBody UserRelationDto dto){
        return apUserService.userFollow(dto);
    }

}
