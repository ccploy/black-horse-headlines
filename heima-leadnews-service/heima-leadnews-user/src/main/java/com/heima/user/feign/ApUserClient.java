package com.heima.user.feign;

import com.heima.apis.user.IApUserClient;
import com.heima.model.admin.dtos.AuthDto;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.user.pojos.ApUser;
import com.heima.user.service.ApUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author CC
 * time 2024-06-04
 * description
 */
@RestController
public class ApUserClient implements IApUserClient {

    @Autowired
    private ApUserService apUserService;

    @Override
    @PostMapping("/api/v1/auth/list")
    public ResponseResult listApUsers(@RequestBody AuthDto dto) {
        return apUserService.getList(dto);
    }

    @Override
    public ResponseResult authFail(AuthDto dto) {
       return apUserService.authFail(dto);
    }

    @Override
    public ApUser findUserById(Integer id) {
       return apUserService.getById(id);
    }

    @Override
    public ResponseResult authPass(AuthDto dto) {
       return apUserService.authPass(dto);
    }


}
