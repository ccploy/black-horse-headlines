package com.heima.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.user.pojos.ApUserFan;
import com.heima.model.user.pojos.ApUserFollow;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author CC
 * time 2024-06-08
 * description
 */
@Mapper
public interface ApUserFanMapper extends BaseMapper<ApUserFan> {
}
