package com.heima.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.user.pojos.ApUserRealname;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author CC
 * time 2024-06-04
 * description
 */
@Mapper
public interface ApUserRealnameMapper extends BaseMapper<ApUserRealname> {
}
