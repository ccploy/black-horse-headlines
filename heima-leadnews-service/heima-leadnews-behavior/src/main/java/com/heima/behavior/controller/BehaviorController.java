package com.heima.behavior.controller;

import com.heima.behavior.service.BehaviorService;
import com.heima.model.behavior.dtos.*;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author CC
 * time 2024-06-08
 * description
 */
@RestController
@RequestMapping("/api/v1")
public class BehaviorController {

   @Autowired
   private BehaviorService behaviorService;


   @PostMapping("/likes_behavior")
    public ResponseResult likes(@RequestBody LikesBehaviorDto dto){
        return behaviorService.likes(dto);
   }
   @PostMapping("/read_behavior")
    public ResponseResult read(@RequestBody ReadBehaviorDto dto){
       return behaviorService.read(dto);
   }
   @PostMapping("/un_likes_behavior`")
    public ResponseResult unlikes(@RequestBody UnLikesBehaviorDto dto){
       return behaviorService.unlikes(dto);
   }






}
