package com.heima.behavior.service;

import com.heima.model.behavior.dtos.*;
import com.heima.model.common.dtos.ResponseResult;

/**
 * @author CC
 * time 2024-06-08
 * description
 */

public interface BehaviorService {
    ResponseResult likes(LikesBehaviorDto dto);

    ResponseResult read(ReadBehaviorDto dto);

    ResponseResult unlikes(UnLikesBehaviorDto dto);

}
