package com.heima.article.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.article.pojos.ApArticleContent;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author CC
 * time 2024-05-06
 * description
 */
@Mapper
public interface ApArticleContentMapper extends BaseMapper<ApArticleContent> {
}
