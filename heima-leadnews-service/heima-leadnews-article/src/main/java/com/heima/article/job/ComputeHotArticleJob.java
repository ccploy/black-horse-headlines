package com.heima.article.job;

import com.heima.article.service.HotArticleService;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author CC
 * time 2024-06-08
 * description
 */

@Slf4j
@Component
public class ComputeHotArticleJob {
    @Autowired
    private HotArticleService hotArticleService;

    @XxlJob(value = "computeHotArticleJob")
    public void computed(){
        log.info("定时计算热点文章任务开始执行");
        hotArticleService.computedHotArticle();

        log.info("定时计算热点文章任务执行结束");
    }



}
