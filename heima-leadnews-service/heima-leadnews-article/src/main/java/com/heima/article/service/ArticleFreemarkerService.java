package com.heima.article.service;

import com.heima.model.article.pojos.ApArticle;

/**
 * @author CC
 * time 2024-05-07
 * description
 */
public interface ArticleFreemarkerService {

    void buildArticleToMinio(ApArticle apArticle,String content);

}
