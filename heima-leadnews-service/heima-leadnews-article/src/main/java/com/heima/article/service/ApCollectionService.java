package com.heima.article.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.article.pojos.ApCollection;
import com.heima.model.behavior.dtos.CollectionBehaviorDto;
import com.heima.model.common.dtos.ResponseResult;

/**
 * @author CC
 * time 2024-06-08
 * description
 */
public interface ApCollectionService extends IService<ApCollection> {
    ResponseResult collection(CollectionBehaviorDto dto);
}
