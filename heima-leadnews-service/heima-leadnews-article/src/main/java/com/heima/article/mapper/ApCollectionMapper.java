package com.heima.article.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.article.pojos.ApCollection;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author CC
 * time 2024-06-08
 * description
 */
@Mapper
public interface ApCollectionMapper extends BaseMapper<ApCollection> {

}
