package com.heima.article.listen;

import com.alibaba.fastjson.JSON;
import com.heima.article.service.ApArticleConfigService;
import com.heima.common.constants.WmNewsMessageConstants;
import com.heima.model.article.pojos.ApArticleConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Map;

/**
 * @author CC
 * time 2024-05-09
 * description
 */
@Slf4j
@Component
public class ArticleIsDownListen {
    @Autowired
    private ApArticleConfigService apArticleConfigService;


    @KafkaListener(topics = {WmNewsMessageConstants.WM_NEWS_UP_OR_DOWN_TOPIC})
    public void onMessage(String message){
        if (StringUtils.hasText(message)) {
            Map<String,Object> map = JSON.parseObject(message, Map.class);
            Long articleId = (Long) map.get("articleId");
            Integer enable = (Integer) map.get("enable");
            ApArticleConfig apArticleConfig = new ApArticleConfig();
            apArticleConfig.setArticleId(articleId);
            apArticleConfig.setIsDown(enable==0?false:true);
            apArticleConfigService.updateById(apArticleConfig);
        }
    }

}
