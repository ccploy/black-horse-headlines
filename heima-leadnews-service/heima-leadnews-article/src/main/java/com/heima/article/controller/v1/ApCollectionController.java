package com.heima.article.controller.v1;

import com.heima.article.service.ApCollectionService;
import com.heima.model.behavior.dtos.CollectionBehaviorDto;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author CC
 * time 2024-06-08
 * description
 */
@RestController
public class ApCollectionController {

    @Autowired
    private ApCollectionService apCollectionService;

    @PostMapping("/api/v1/collection_behavior")
    public ResponseResult collection(@RequestBody CollectionBehaviorDto dto){
        return apCollectionService.collection(dto);
    }

}
