package com.heima.article.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.article.pojos.ApArticleConfig;
import com.heima.model.comment.dtos.CommentConfigDto;
import com.heima.model.common.dtos.ResponseResult;

/**
 * @author CC
 * time 2024-05-09
 * description
 */
public interface ApArticleConfigService extends IService<ApArticleConfig> {
    ResponseResult updateCommentStatus(CommentConfigDto dto);
}
