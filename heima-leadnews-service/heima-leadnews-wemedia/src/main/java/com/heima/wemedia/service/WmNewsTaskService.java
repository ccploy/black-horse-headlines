package com.heima.wemedia.service;

import java.util.Date;

/**
 * @author CC
 * time 2024-05-08
 * description
 */
public interface WmNewsTaskService {
    void addNesToTask(Integer id, Date publishTime);
    void scanNewsByTask();
}
