package com.heima.wemedia.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.model.admin.pojos.AdChannel;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.dtos.ChannelDto;
import com.heima.model.wemedia.dtos.WmNewsDto;
import com.heima.model.wemedia.pojos.WmChannel;
import com.heima.model.wemedia.pojos.WmNews;
import com.heima.model.wemedia.pojos.WmUser;
import com.heima.utils.thread.WmThreadLocal;
import com.heima.wemedia.mapper.WmChannelMapper;
import com.heima.wemedia.mapper.WmNewsMaterialMapper;
import com.heima.wemedia.service.WmChannelService;
import com.heima.wemedia.service.WmNewsService;
import com.sun.org.apache.bcel.internal.generic.NEW;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Transactional
@Slf4j
public class WmChannelServiceImpl extends ServiceImpl<WmChannelMapper, WmChannel> implements WmChannelService {

    @Autowired
    private WmNewsMaterialMapper wmNewsMaterialMapper;

    @Override
    public ResponseResult del(Integer id) {
        if (id==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        WmChannel one = getById(id);
        if (one==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        if (one.getStatus()) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_IS_USING);
        }
        //删除自媒体的频道信息
        removeById(id);
        //删除管理端
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    @Override
    public ResponseResult saveData(AdChannel data) {
        if (data==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        String name = data.getName();
        if (StringUtils.isEmpty(name)) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        WmChannel one = getOne(Wrappers.lambdaQuery(WmChannel.class)
                .eq(WmChannel::getName, name));
        if (one!=null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_EXIST);
        }
        one.setCreatedTime(new Date());
        save(one);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    @Override
    public ResponseResult getList(ChannelDto dto) {
        if (dto==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        Page<WmChannel> page = page(new Page<WmChannel>(
                Optional.ofNullable(dto.getPage()).orElse(1),
                Optional.ofNullable(dto.getSize()).orElse(5)
        ), Wrappers.lambdaQuery(WmChannel.class)
                .like(StringUtils.hasText(dto.getName()), WmChannel::getName, dto.getName())
                .eq(dto.getStatus()!=null&&dto.getStatus()<=1&&dto.getStatus()>=0,WmChannel::getStatus,dto.getStatus()==1)
                .orderByDesc(WmChannel::getCreatedTime));
        PageResponseResult res = new PageResponseResult(((int) page.getCurrent()), ((int) page.getSize()), ((int) page.getTotal()));
        return res.ok(page.getRecords().stream().map(e->{
            AdChannel adChannel = new AdChannel();
            BeanUtils.copyProperties(e,adChannel);
            return adChannel;
        }).collect(Collectors.toList()));
    }

    @Override
    public ResponseResult updateData(AdChannel data) {

        if (data==null || data.getId()==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        String name = data.getName();
        if (StringUtils.isEmpty(name)) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        WmChannel wmChannel = new WmChannel();
        BeanUtils.copyProperties(data,wmChannel);
        updateById(wmChannel);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    @Override
    public ResponseResult findAll() {
       return ResponseResult.okResult(list());
    }




}