package com.heima.wemedia.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.wemedia.pojos.WmNewsMaterial;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface WmNewsMaterialMapper extends BaseMapper<WmNewsMaterial> {
     /**
      * 保存图文关联
      * @param materialIds 素材id
      * @param newsId 文章id
      * @param type 引用类型 0内容引用 1图文引用
      */
     void saveRelations(@Param("materialIds") List<Integer> materialIds,
                        @Param("newsId") Integer newsId,
                        @Param("type")Short type);

}