package com.heima.wemedia.service;

/**
 * @author CC
 * time 2024-05-07
 * description
 */
public interface WmNewsAutoScanService {

    void autoScanWmNews(Integer id);
}
