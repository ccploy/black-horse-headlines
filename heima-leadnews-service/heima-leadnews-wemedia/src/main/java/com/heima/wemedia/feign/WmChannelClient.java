package com.heima.wemedia.feign;

import com.heima.apis.wemedia.IWmChannelClient;
import com.heima.model.admin.pojos.AdChannel;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.ChannelDto;
import com.heima.wemedia.service.WmChannelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author CC
 * time 2024-06-04
 * description
 */
@RestController
public class WmChannelClient implements IWmChannelClient {
    @Autowired
    private WmChannelService wmChannelService;

    @Override
    @PostMapping("/api/v1/channel/list")
    public ResponseResult list(@RequestBody ChannelDto dto) {

        return wmChannelService.getList(dto);
    }

    @Override
    @PostMapping("/api/v1/channel/save")
    public ResponseResult save(@RequestBody AdChannel data) {
        return wmChannelService.saveData(data);
    }

    @Override
    @PostMapping("/api/v1/channel/update")
    public ResponseResult update(@RequestBody AdChannel data) {

        return wmChannelService.updateData(data);
    }

    @Override
    public ResponseResult list() {
       return ResponseResult.okResult(wmChannelService.list());
    }

    @Override
    @GetMapping("/api/v1/channel/del/{id}")
    public ResponseResult del(@PathVariable("id") Integer id) {
        return wmChannelService.del(id);
    }
}
