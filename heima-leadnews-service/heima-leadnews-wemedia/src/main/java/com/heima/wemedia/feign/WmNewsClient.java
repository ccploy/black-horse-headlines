package com.heima.wemedia.feign;

import com.heima.apis.wemedia.IWmNewsClient;
import com.heima.model.admin.dtos.AuthDto;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.NewsAuthDto;
import com.heima.wemedia.service.WmNewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author CC
 * time 2024-06-05
 * description
 */
@RestController
public class WmNewsClient implements IWmNewsClient {

    @Autowired
    private WmNewsService wmNewsService;

    @Override
    @PostMapping("/api/v1/auth/authFail")
    public ResponseResult authFail(@RequestBody NewsAuthDto dto) {
        return wmNewsService.authFail(dto);
    }

    @Override
    public ResponseResult getList(NewsAuthDto dto) {
       return wmNewsService.getList(dto);
    }

    @Override
    public ResponseResult showDetails(Integer id) {
       return wmNewsService.showDetails(id);
    }

    @Override
    public ResponseResult authPass(NewsAuthDto dto) {
        return wmNewsService.authPass(dto);
    }
}
