package com.heima.wemedia.controller.v1;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.dtos.WmMaterialDto;
import com.heima.model.wemedia.pojos.WmMaterial;
import com.heima.utils.thread.WmThreadLocal;
import com.heima.wemedia.service.WmMaterialService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author CC
 * time 2024-05-06
 * description
 */
@Api(tags = "自媒体端-素材管理")
@RestController
@RequestMapping("/api/v1/material")
public class WmMaterialController {
    @Autowired
    private WmMaterialService wmMaterialService;

    @ApiOperation("上传图片")
    @PostMapping("/upload_picture")
    public ResponseResult uploadPicture(@RequestBody MultipartFile multipartFile){
        return wmMaterialService.uploadPicture(multipartFile);
    }
    @ApiOperation("获取素材列表")
    @PostMapping("/list")
    public ResponseResult list(@RequestBody WmMaterialDto dto){
        return wmMaterialService.findList(dto);
    }

    @ApiOperation("素材删除")
    @GetMapping("/del_picture/{id}")
    public ResponseResult delPictureById(@PathVariable("id") Integer id) {
        if(id==null || id<=0){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        return wmMaterialService.delPictureById(id);
    }
    @ApiOperation("素材收藏")
    @GetMapping("/collect/{id}")
    public ResponseResult collect(@PathVariable("id") Integer id){
        return wmMaterialService.collect(id);
    }
    @ApiOperation("素材取消收藏")
    @GetMapping("/cancel_collect/{id}")
    public ResponseResult cancel_collect(@PathVariable("id") Integer id){
        return wmMaterialService.cancel_collect(id);
    }
}
