package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.admin.dtos.SensitiveDto;
import com.heima.model.admin.pojos.AdSensitive;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.pojos.WmSensitive;

/**
 * @author CC
 * time 2024-06-04
 * description
 */
public interface WmSensitiveService extends IService<WmSensitive> {


    ResponseResult del(Integer id);

    ResponseResult getList(SensitiveDto dto);

    ResponseResult saveData(AdSensitive data);

    ResponseResult updateData(AdSensitive adSensitive);
}
