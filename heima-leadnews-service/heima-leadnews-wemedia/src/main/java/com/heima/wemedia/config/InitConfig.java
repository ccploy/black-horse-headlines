package com.heima.wemedia.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author CC
 * time 2024-05-07
 * description
 */
@Configuration
@ComponentScan(basePackages = {"com.heima.apis.article.fallback"})
public class InitConfig {

}
