package com.heima.wemedia.service.impl;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.model.admin.dtos.SensitiveDto;
import com.heima.model.admin.pojos.AdSensitive;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.pojos.WmSensitive;
import com.heima.wemedia.mapper.WmSensitiveMapper;
import com.heima.wemedia.service.WmSensitiveService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

/**
 * @author CC
 * time 2024-06-04
 * description
 */
@Service
public class WmSensitiveServiceImpl extends ServiceImpl<WmSensitiveMapper, WmSensitive> implements WmSensitiveService {
    @Override
    public ResponseResult del(Integer id) {
        if (id==null || id<=0) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        removeById(id);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    @Override
    public ResponseResult getList(SensitiveDto dto) {
        if (dto==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        Page<WmSensitive> page = page(new Page<WmSensitive>(Optional.ofNullable(dto.getPage()).orElse(1),
                        Optional.ofNullable(dto.getSize()).orElse(5)),
                Wrappers.lambdaQuery(WmSensitive.class)
                        .like(StringUtils.isNotBlank(dto.getName()), WmSensitive::getSensitives, dto.getName())
                        .orderByDesc(WmSensitive::getCreatedTime));
        PageResponseResult res = new PageResponseResult(((int) page.getCurrent()), ((int) page.getSize()), ((int) page.getTotal()));
        return res.ok(page.getRecords());
    }

    @Override
    public ResponseResult saveData(AdSensitive data) {
        if (data==null || StringUtils.isBlank(data.getSensitives())) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //查询是否存在
        WmSensitive one = getOne(Wrappers.lambdaQuery(WmSensitive.class).like(WmSensitive::getSensitives, data.getSensitives()));
        if (one!=null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_EXIST,"该敏感词已存在");
        }
        WmSensitive wmSensitive = new WmSensitive();
        wmSensitive.setSensitives(data.getSensitives());
        wmSensitive.setCreatedTime(new Date());
        save(wmSensitive);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    @Override
    public ResponseResult updateData(AdSensitive adSensitive) {
        if (adSensitive==null || adSensitive.getId()==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        WmSensitive one = getById(adSensitive.getId());
        if (one==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        one.setSensitives(adSensitive.getSensitives());
        updateById(one);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }
}
