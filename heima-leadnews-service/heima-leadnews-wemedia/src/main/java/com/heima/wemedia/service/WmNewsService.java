package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.admin.dtos.AuthDto;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.NewsAuthDto;
import com.heima.model.wemedia.dtos.WmNewsDto;
import com.heima.model.wemedia.dtos.WmNewsPageReqDto;
import com.heima.model.wemedia.pojos.WmNews;

import java.util.Map;

public interface WmNewsService extends IService<WmNews> {


    ResponseResult findAll(WmNewsPageReqDto dto);
    ResponseResult submitNews(WmNewsDto dto);

    ResponseResult showDetails(Integer id);

    ResponseResult del_news(Integer id);

    ResponseResult down_or_up(Map<String,Object> dto);

    ResponseResult authFail(NewsAuthDto dto);

    ResponseResult authPass(NewsAuthDto dto);

    ResponseResult getList(NewsAuthDto dto);
}
