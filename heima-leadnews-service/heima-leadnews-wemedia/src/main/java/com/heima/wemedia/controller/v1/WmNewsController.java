package com.heima.wemedia.controller.v1;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.WmNewsDto;
import com.heima.model.wemedia.dtos.WmNewsPageReqDto;
import com.heima.wemedia.service.WmNewsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author CC
 * time 2024-05-06
 * description
 */

@Api(tags = "自媒体文章")
@RestController
@RequestMapping("/api/v1/news")
public class WmNewsController {

    @Autowired
    private WmNewsService wmNewsService;

    @PostMapping("/list")
    public ResponseResult findAll(@RequestBody WmNewsPageReqDto dto){
        return wmNewsService.findAll(dto);
    }
    @PostMapping("/submit")
    public ResponseResult submit(@RequestBody WmNewsDto dto){
        return wmNewsService.submitNews(dto);
    }
    @ApiOperation("查看详情")
    @GetMapping("/one/{id}")
    public ResponseResult showDetails(@PathVariable("id") Integer id){
        return wmNewsService.showDetails(id);
    }

    @ApiOperation("文章删除")
    @GetMapping("/del_news/{id}")
    public ResponseResult del_news(@PathVariable("id") Integer id){
        return wmNewsService.del_news(id);
    }

    @ApiOperation("文章上下架")
    @PostMapping("/down_or_up")
    public ResponseResult down_or_up(@RequestBody Map<String,Object> dto){
        return wmNewsService.down_or_up(dto);
    }


}
