package com.heima.wemedia.feign;

import com.heima.apis.wemedia.IWmSensitiveClient;
import com.heima.model.admin.dtos.SensitiveDto;
import com.heima.model.admin.pojos.AdSensitive;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.wemedia.service.WmSensitiveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author CC
 * time 2024-06-04
 * description
 */
@RestController
public class WmSensitiveClient implements IWmSensitiveClient {
    @Autowired
    private WmSensitiveService wmSensitiveService;

    @Override
    @DeleteMapping("/api/v1/sensitive/del/{id}")
    public ResponseResult del(Integer id) {
        return wmSensitiveService.del(id);
    }

    @Override
    @PostMapping("/api/v1/sensitive/list")
    public ResponseResult list(SensitiveDto dto) {
        return wmSensitiveService.getList(dto);
    }

    @Override
    @PostMapping("/api/v1/sensitive/update")
    public ResponseResult update(AdSensitive adSensitive) {
        return wmSensitiveService.updateData(adSensitive);
    }

    @Override
    @PostMapping("/api/v1/sensitive/save")
    public ResponseResult save(AdSensitive adSensitive) {
        return wmSensitiveService.saveData(adSensitive);
    }


}
