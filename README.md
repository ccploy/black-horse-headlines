# 黑马头条

#### 介绍
随着智能手机的普及，人们更加习惯于通过手机来看新闻。由于生活节奏的加快，
很多人只能利用碎片时间来获取信息，因此，对于移动资讯客户端的需求也越来越高。
黑马头条项目正是在这样背景下开发出来。黑马头条项目采用当下火热的微服务+大数据技术架构实现。
本项目主要着手于获取最新最热新闻资讯，通过大数据分析用户喜好精确推送咨询新闻

#### 软件架构
软件架构说明
Spring-Cloud-Gateway : 微服务之前架设的网关服务，实现服务注册中的API请求路由，以及控制流速控制和熔断处理都是常用的架构手段，而这些功能Gateway天然支持
运用Spring Boot快速开发框架，构建项目工程；并结合Spring Cloud全家桶技术，实现后端个人中心、自媒体、管理中心等微服务。
运用Spring Cloud Alibaba Nacos作为项目中的注册中心和配置中心
运用mybatis-plus作为持久层提升开发效率
运用Kafka完成内部系统消息通知；与客户端系统消息通知；以及实时数据计算
运用Redis缓存技术，实现热数据的计算，提升系统性能指标
使用Mysql存储用户数据，以保证上层数据查询的高性能
使用Mongo存储用户热数据，以保证用户热数据高扩展和高性能指标
使用FastDFS作为静态资源存储器，在其上实现热静态资源缓存、淘汰等功能
运用Hbase技术，存储系统中的冷数据，保证系统数据的可靠性
运用ES搜索技术，对冷数据、文章数据建立索引，以保证冷数据、文章查询性能
运用AI技术，来完成系统自动化功能，以提升效率及节省成本。比如实名认证自动化
PMD&P3C : 静态代码扫描工具，在项目中扫描项目代码，检查异常点、优化点、代码规范等，为开发团队提供规范统一，提升项目代码质量

#### 安装教程

1.  根据资料的镜像搭建并启动虚拟机服务 
2.  mysql数据库运行leadnews_xxx.sql脚本
3.  docker启动redis 和 nacos
4.  本地或虚拟机配置并启动nginx
   1. nginx -t 测试配置文件是否可用 

#### 使用说明
  ### 1.项目模块说明
1.  heima-leadnews-common : 项目公共模块
2.  heima-leadnews-feign-api : 项目远程调用模块
3.  heima-leadnews-gateway : 项目网关模块
        - app-gateway : 客户端网关模块 进行登录认证
        - admin-gateway : 后台网关模块 进行登录认证
        - wemedia-gateway : 自媒体网关模块 进行登录认证
4.  heima-leadnews-model : 项目实体类模块
        - user : 用户实体类
        - common : 公共实体类
5.  heima-leadnews-service : 项目业务模块
        - heima-leadnews-user : 用户业务模块
        - heima-leadnews-article : 文章业务模块
        - heima-leadnews-wemedia : 自媒体业务模块
        - heima-leadnews-behavior : 用户行为业务模块
        - heima-leadnews-search : 搜索业务模块
        - heima-leadnews-admin : 后台管理模块
6.  heima-leadnews-test : 项目测试模块
        - freemarker-demo : freemarker模板引擎使用示例
        - minio-demo : minio文件存储引擎使用示例
        - es-init : es搜索引擎初始化数据示例
        - mongo-demo : mongo数据库使用示例
        - tess4j-demo : tess4j图像识别使用示例
7.  heima-leadnews-utils : 项目工具类模块
8.  heima-leadnews-basic : 项目基础模块,存放自定义starter


  ### 2.虚拟机说明
1.环境IP：192.168.200.130
2.子网IP：192.168.200.0
3.账号: root 密码：itcast
4.redis密码：leadnews
5.minio账号：minio  密码：minio123 服务端口9000
6.xxl账号: admin 密码: 123456  http://192.168.200.130:8888/xxl-job-admin/

  ### 3.项目初始化问题BUG解决
1. maven安装路径 和 maven包问题 , 必须使用资料提供的maven仓库
2. 将父工程POM的repositories仓库标签全注释 , 父pom依赖的jackson-dataformat-cbor版本写死2.9.9
3. logback的日志路径使用/
4. 若提示命令行过长,无法启动,则设置启动参数配置shorten-Command Line 改为 classpath file / jar...  或者 减短启动类方法名
5. docker运行redis 或 将启动redis的命令注释并加上@SpringBootApplication(exclude = RedisAutoConfiguration.class)
6. 数据库连接参数 &useSSL=false
7. 若项目出现异常,可考虑删除项目缓存, target目录,然后刷新maven,重新加载
8. 使用heima-file-starter模块前,需要进行maven install
9. 注意Nacos配置中的IP和端口以及密码配置等 
10. 分布式事务seata配置 docker exec -it seata /bin/sh , 按需修改file.conf 和 registry.conf , 注意IP


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
