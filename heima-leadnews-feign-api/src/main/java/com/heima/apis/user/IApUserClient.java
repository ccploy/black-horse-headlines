package com.heima.apis.user;

import com.heima.model.admin.dtos.AuthDto;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.user.pojos.ApUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author CC
 * time 2024-06-04
 * description
 */
@FeignClient(value = "leadnews_user")
public interface IApUserClient {

    @PostMapping("/api/v1/auth/list")
    ResponseResult listApUsers(@RequestBody AuthDto dto);

    @PostMapping("/api/v1/auth/authFail")
    ResponseResult authFail(@RequestBody AuthDto dto);

    @PostMapping("/api/v1/auth/authPass")
    ResponseResult authPass(@RequestBody AuthDto dto);

    @GetMapping("/api/v1/user/{id}")
    ApUser findUserById(@PathVariable("id") Integer id);


}
