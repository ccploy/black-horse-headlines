package com.heima.apis.wemedia;

import com.heima.model.admin.dtos.SensitiveDto;
import com.heima.model.admin.pojos.AdSensitive;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author CC
 * time 2024-06-04
 * description
 */
@FeignClient(value="leadnews-wemedia")
public interface IWmSensitiveClient {

     @PostMapping("/api/v1/sensitive/del/{id}")
    ResponseResult del(@PathVariable("id")Integer id);

     @PostMapping("/api/v1/sensitive/list")
    ResponseResult list(@RequestBody SensitiveDto dto);

     @PostMapping("/api/v1/sensitive/save")
    ResponseResult save(@RequestBody AdSensitive adSensitive);

     @PostMapping("/api/v1/sensitive/update")
    ResponseResult update(@RequestBody AdSensitive adSensitive);


}
