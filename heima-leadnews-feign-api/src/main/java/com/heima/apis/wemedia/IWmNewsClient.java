package com.heima.apis.wemedia;

import com.heima.model.admin.dtos.AuthDto;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.NewsAuthDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author CC
 * time 2024-06-04
 * description
 */
@FeignClient(value = "leadnews-wemedia")
public interface IWmNewsClient {

    @PostMapping("/api/v1/auth/authFail")
    ResponseResult authFail(@RequestBody NewsAuthDto dto);

    @PostMapping("/api/v1/auth/authPass")
    ResponseResult authPass(@RequestBody NewsAuthDto dto);
    @GetMapping("/api/v1/news/one_vo/{id}")
    ResponseResult showDetails(@PathVariable("id") Integer id);

    @PostMapping("/api/v1/news/list_vo")
    ResponseResult getList(@RequestBody NewsAuthDto dto);
}
