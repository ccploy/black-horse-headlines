package com.heima.apis.wemedia;

import com.heima.model.admin.pojos.AdChannel;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.ChannelDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author CC
 * time 2024-06-04
 * description
 */
@FeignClient(value = "leadnews-wemedia")
public interface IWmChannelClient {

    @GetMapping("/api/v1/channel/del/{id}")
    public ResponseResult del(@PathVariable("id") Integer id);
    @PostMapping("/api/v1/channel/list")
    ResponseResult list(@RequestBody ChannelDto dto);

    @PostMapping("/api/v1/channel/save")
    ResponseResult save(@RequestBody AdChannel data);

    @PostMapping("/api/v1/channel/update")
    ResponseResult update(@RequestBody AdChannel data);

    @GetMapping("/api/v1/channel/list")
    ResponseResult list();


}
