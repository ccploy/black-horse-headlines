package com.heima.es.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.es.pojos.SearchArticleVo;
import com.heima.model.article.pojos.ApArticle;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author CC
 * time 2024-06-03
 * description
 */
@Mapper
public interface ApArticleMapper extends BaseMapper<ApArticle> {

    List<SearchArticleVo> loadArticleList();

}
