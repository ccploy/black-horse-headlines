package com.heima.es.pojos;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author CC
 * time 2024-06-03
 * description
 */
@Data
public class SearchArticleVo implements Serializable {
    private static final long serialVersionUID =1L;

    private Long id;

    private String title;

    private Date publishTime;

    private Integer layout;

    private String images;

    private Long authorId;
    private String authorName;
    private String staticUrl;
    private String content;


}
