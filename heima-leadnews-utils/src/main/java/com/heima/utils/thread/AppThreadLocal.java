package com.heima.utils.thread;

import com.heima.model.user.pojos.ApUser;

/**
 * @author CC
 * time 2024-05-06
 * description
 */
public class AppThreadLocal {
    private final static ThreadLocal<ApUser> WM_USER_THREAD_LOCAL=new ThreadLocal<>();

    public static void setUser(ApUser wmUser){
        WM_USER_THREAD_LOCAL.set(wmUser);
    }

    public static ApUser getUser(){
        return WM_USER_THREAD_LOCAL.get();
    }

    public static void clear(){
        WM_USER_THREAD_LOCAL.remove();
    }

}
