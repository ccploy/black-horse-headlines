package com.heima.common.constants;

/**
 * @author CC
 * time 2024-06-08
 * description
 */
public class BehaviorConstants {

    public static final String USER_BEHAVIOR_LIKES = "behavior:user:likes:";


    public static final String USER_BEHAVIOR_READ = "behavior:user:read:";
    public static final String USER_BEHAVIOR_UNLIKES = "behavior:user:unlikes:";
    public static final String USER_BEHAVIOR_FOLLOW = "behavior:user:follow:";
    public static final String USER_BEHAVIOR_COLLECTION = "behavior:user:collection:";
    public static final String APUSER_FOLLOW_RELATION = "behavior:apuser:follow:relation:";
    public static final String APUSER_FANS_RELATION = "behavior:apuser:fans:relation:";
}
